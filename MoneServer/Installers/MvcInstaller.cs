﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using MoneServer.Data;
using MoneServer.Services;
using Swashbuckle.AspNetCore.Swagger;

namespace MoneServer.Installers
{
    public class MvcInstaller : IInstaller
    {
        public void InstallService(IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

        


            

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
           
            services.TryAddScoped<FileUploadService>();

            services.AddSwaggerGen(
                x => { x.SwaggerDoc("v1", new Info { Title = "Mone Api", Version = "v1" }); });
        }
    }
}

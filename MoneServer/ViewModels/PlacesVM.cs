﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MoneServer.Data.Entities;

namespace MoneServer.ViewModels
{
    public class PlacesVM
    {
        public IEnumerable<Place> Places { get; set; }
        public PageViewModel PageViewModel { get; set; }

    }
    public class PageInfo
    {
        public int PageNumber { get; set; } // номер текущей страницы
        public int PageSize { get; set; } // кол-во объектов на странице
        public int TotalItems { get; set; } // всего объектов
        public int TotalPages  // всего страниц
        {
            get { return (int)Math.Ceiling((decimal)TotalItems / PageSize); }
        }
    }
    public class IndexViewModel
    {
        public IEnumerable<Place> Places { get; set; }
        public PageInfo PageInfo { get; set; }
    }
}

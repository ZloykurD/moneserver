﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace MoneServer.ViewModels
{
    public class CreatePlaceVM
    {
        [Required(ErrorMessage = "Name")]
        [Display(Name = "Name")]
        public String Name { get; set; }

        [Required(ErrorMessage = "Description")]
        [Display(Name = "Description")]
        public String Description { get; set; }

        [Required(ErrorMessage = "Avatar")]
        [Display(Name = "Avatar")]
        public IFormFile PlaceAvatar { get; set; }

        [Required(ErrorMessage = "Gallery")]
        [Display(Name = "Gallery")]
        public IList<IFormFile> PlaceGallery { get; set; }
    }
}
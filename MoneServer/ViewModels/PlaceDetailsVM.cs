﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MoneServer.Data.Entities;
using MoneServer.Domain;

namespace MoneServer.ViewModels
{
    public class PlaceDetailsVM
    {

        public String Id { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }
        public DateTime CreateAt { get; set; }
        public String ImagePath { get; set; }
        public double Rating { get; set; }

        public IEnumerable<Gallery> Galleries { get; set; }
        public IEnumerable<Feedback> Feedbacks { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoneServer.Options
{
    public class SwaggerOptions
    {
        public String JsonRoute { get; set; }
        public String Description { get; set; }
        public String UIEndpoint { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MoneServer.Data.Entities;
using MoneServer.Domain;

namespace MoneServer.Data.Configurations
{
    public class GalleryConfiguration : IEntityTypeConfiguration<Gallery>
    {
        public void Configure(EntityTypeBuilder<Gallery> builder)
        {
            builder.HasOne<Place>(u => u.Parent)
                .WithMany(u => u.Galleries)
                .HasForeignKey(u => u.ParentId)
                .OnDelete(DeleteBehavior.SetNull);
        }
    }
}
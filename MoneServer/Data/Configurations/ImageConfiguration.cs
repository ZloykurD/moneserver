﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MoneServer.Data.Entities;
using MoneServer.Domain;

namespace MoneServer.Data.Configurations
{
    public class ImageConfiguration : IEntityTypeConfiguration<Image>
    {
        public void Configure(EntityTypeBuilder<Image> builder)
        {
            builder.HasOne<AppliactionUser>(u => u.Parent).WithMany(u => u.Images).HasForeignKey(u => u.ParentId)
                .OnDelete(DeleteBehavior.SetNull);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MoneServer.Data.Entities;
using MoneServer.Domain;

namespace MoneServer.Data.Configurations
{
    public class PlaceConfiguration : IEntityTypeConfiguration<Place>
    {
        public void Configure(EntityTypeBuilder<Place> builder)
        {

            builder.HasOne<AppliactionUser>(x => x.User).WithMany(x => x.Places).HasForeignKey(x => x.UserId).OnDelete(DeleteBehavior.SetNull);

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MoneServer.Data.Entities;
using MoneServer.Domain;

namespace MoneServer.Data.Configurations
{
    public class FeedbackConfiguration : IEntityTypeConfiguration<Feedback>
    {
        public void Configure(EntityTypeBuilder<Feedback> builder)
        {
            builder.HasOne<AppliactionUser>(x => x.User).WithMany(x => x.Feedbacks).HasForeignKey(x=>x.UserId).OnDelete(DeleteBehavior.SetNull);
        }
    }
}

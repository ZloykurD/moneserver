﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MoneServer.Domain;

namespace MoneServer.Data.Entities
{
    public class Image : Entity
    {
        public String ImagePath { get; set; }
        public String ParentId { get; set; }
        public AppliactionUser Parent { get; set; }


    }
}
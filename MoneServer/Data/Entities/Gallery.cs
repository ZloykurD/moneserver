﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoneServer.Data.Entities
{
    public class Gallery:Entity
    {
        public String ImagePath { get; set; }
        public String ParentId { get; set; }
        public Place Parent { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MoneServer.Domain;

namespace MoneServer.Data.Entities
{
    public class Feedback : Entity
    {
        public String Text { get; set; }
        public String PlaceId { get; set; }
        public Place Place { get; set; }
        public String UserId { get; set; }
        public AppliactionUser User { get; set; }
        public double Rating { get; set; }
        public DateTime CreateAt { get; set; }
    }
}

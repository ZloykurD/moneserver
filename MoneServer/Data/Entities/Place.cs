﻿using System;
using System.Collections.Generic;
using MoneServer.Domain;

namespace MoneServer.Data.Entities
{
    public class Place: Entity
    {
        public String Name { get; set; }
        public String Description { get; set; }
        public String UserId { get; set; }
        public AppliactionUser User { get; set; }
        public DateTime CreateAt { get; set; }
        public String ImagePath { get; set; }
        public double Rating { get; set; }

        public IEnumerable<Gallery> Galleries { get; set; }
        public IEnumerable<Feedback> Feedbacks { get; set; }
    }
}

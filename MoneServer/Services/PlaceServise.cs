﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MoneServer.Data;
using MoneServer.Data.Entities;
using MoneServer.Repositories;

namespace MoneServer.Services
{
    public class PlaceServise : Repository<Place>, IPlaceServise
    {
        public PlaceServise(ApplicationDbContext context) : base(context)
        {
            All = context.Places;
        }
    }
}

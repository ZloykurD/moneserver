﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MoneServer.Data;
using MoneServer.Data.Entities;
using MoneServer.Repositories;

namespace MoneServer.Services
{
    public class ImageService : Repository<Image>, IImageServise
    {
        public ImageService(ApplicationDbContext context) : base(context)
        {
            All = context.Images;
        }
    }
}

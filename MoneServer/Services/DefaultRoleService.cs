﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using MoneServer.Domain;

namespace MoneServer.Services
{
    public class DefaultRoleService
    {
        public async Task SeedAsync(IApplicationBuilder app)
        {
            using (var score = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                var userManager = score.ServiceProvider.GetRequiredService<UserManager<AppliactionUser>>();
                var roleManager = score.ServiceProvider.GetRequiredService<RoleManager<IdentityRole>>();

                if (!await roleManager.RoleExistsAsync("Admin"))
                {
                    IdentityRole roleAdmin = new IdentityRole("Admin");
                    await roleManager.CreateAsync(roleAdmin);
                }

                if (!await roleManager.RoleExistsAsync("User"))
                {
                    IdentityRole roleUser = new IdentityRole("User");
                    await roleManager.CreateAsync(roleUser);
                }


                AppliactionUser admin = await userManager.FindByNameAsync("Admin");
                if (admin == null)
                {
                    var user = new AppliactionUser
                    {
                        UserName = "Admin@mone.space",
                        Email = "Admin@mone.space",
                        ImagePath = "images/admin.png"
                    };

                    var result = await userManager.CreateAsync(user, "Admin321321");
                    if (result.Succeeded)
                    {
                        await userManager.AddToRoleAsync(user, "Admin");
                    }
                }
            }
        }
    }
}
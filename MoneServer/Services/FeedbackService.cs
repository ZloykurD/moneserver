﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MoneServer.Data;
using MoneServer.Data.Entities;
using MoneServer.Repositories;

namespace MoneServer.Services
{
    public class FeedbackService : Repository<Feedback>, IFeedbackServise
    {
        public FeedbackService(ApplicationDbContext context) : base(context)
        {
            All = context.Feedbacks;
        }
    }
}

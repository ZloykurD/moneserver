﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MoneServer.Data;
using MoneServer.Data.Entities;
using MoneServer.Repositories;

namespace MoneServer.Services
{
    public class GalleryService : Repository<Gallery>, IGalleryServise
    {
        public GalleryService(ApplicationDbContext context) : base(context)
        {
            All = context.Galleries;
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Net.Http.Headers;
using MoneServer.Data;
using MoneServer.Data.Entities;
using MoneServer.Domain;
using MoneServer.Repositories;
using MoneServer.Services;
using MoneServer.ViewModels;

namespace MoneServer.Controllers
{
    public class PlacesController : Controller
    {
        //private readonly ApplicationDbContext _context;
        private readonly IPlaceServise _placeServise;
        private readonly IGalleryServise _galleryServise;
        private readonly IFeedbackServise _feedbackServise;
        private readonly IHostingEnvironment _environment;
        private readonly FileUploadService _fileUploadService;
        private readonly UserManager<AppliactionUser> _userManager;

        public PlacesController(
            IGalleryServise galleryServise,
            IPlaceServise placeServise,
            IFeedbackServise feedbackServise,
            UserManager<AppliactionUser> userManager,
            IHostingEnvironment environment,
            FileUploadService fileUploadService)
        {
            _galleryServise = galleryServise;
            _placeServise = placeServise;
            _feedbackServise = feedbackServise;

            _environment = environment;
            _fileUploadService = fileUploadService;

            _userManager = userManager;
        }

        // GET: Places
        public IActionResult Index(int page =1)
        {
            int pageSize = 3;
            int coun =  _placeServise.GetList().ToList().Count();
            IEnumerable<Place> places =  _placeServise.GetList().ToList();

            PageViewModel pageViewModel = new PageViewModel(coun, page, pageSize);
            PlacesVM vm = new PlacesVM
            {
                Places = places.Skip((page - 1) * pageSize).Take(pageSize).ToList(),
                PageViewModel = pageViewModel

            };



            return View(vm);
        }

        // GET: Places/Details/5
        public async Task<IActionResult> Details(string id)
        {
            IEnumerable<Gallery> galleries = null;
            IEnumerable<Feedback> feedbacks = null;
            if (id == null)
            {
                return NotFound();
            }

            Place place = null;
            try
            {
                place = await _placeServise.Find(id);
                galleries = _galleryServise.GetList().Where(x => x.ParentId == id);
                feedbacks = _feedbackServise.GetList().Where(x => x.PlaceId == id);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }


            if (place == null)
            {
                return NotFound();
            }

            PlaceDetailsVM model = new PlaceDetailsVM
            {
                Id = id,
                Name = place.Name,
                ImagePath = place.ImagePath,
                Feedbacks = feedbacks,
                Galleries = galleries,
                Description = place.Description,
                CreateAt = place.CreateAt,
                Rating = place.Rating
            };



            return View(model);
        }

        // GET: Places/Create
        [Authorize]
        public IActionResult Create()
        {
            return View();
        }

        // POST: Places/Create
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CreatePlaceVM input)
        {
            if (ModelState.IsValid)
            {
                var pathAvatar = Path.Combine(_environment.WebRootPath,
                    $"images\\Place\\{_userManager.GetUserId(User)}");
                _fileUploadService.Upload(pathAvatar, input.PlaceAvatar.FileName, input.PlaceAvatar);
                String avatarPath = $"images/Place/{_userManager.GetUserId(User)}/{input.PlaceAvatar.FileName}";


                AppliactionUser user = await _userManager.GetUserAsync(User);
                var place = new Place
                {
                    Name = input.Name,
                    Description = input.Description,
                    ImagePath = avatarPath,
                    CreateAt = DateTime.Now,
                    Rating = 0,
                    User = user,
                    UserId = user.Id
                };

                Place created = await _placeServise.Create(place);
                if (created != null)
                {
                    foreach (IFormFile file in input.PlaceGallery)
                    {
                        var path = Path.Combine(_environment.WebRootPath, $"images\\Gallery\\{user.Id}");
                        _fileUploadService.Upload(path, file.FileName, file);
                        String imagePath = $"images/Gallery/{user.Id}/{file.FileName}";


                        Gallery gallery = new Gallery
                        {
                            Parent = created,
                            ParentId = created.Id,
                            ImagePath = imagePath
                        };

                        await _galleryServise.Create(gallery);
                    }

                    return RedirectToAction(nameof(Index));
                }
            }

            return View(input);
        }

        // GET: Places/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Place place = null;
            try
            {
                place = await _placeServise.Find(id);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return NotFound();
            }

            if (place == null)
            {
                return NotFound();
            }

            return View(place);
        }

        // POST: Places/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("Name,Description,UserId,CreateAt,ImagePath,Rating,Id")]
            Place place)
        {
            if (id != place.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _placeServise.Update(place);
                }
                catch (DbUpdateConcurrencyException)
                {
                    bool x = await PlaceExists(place.Id);
                    if (!x)
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }

                return RedirectToAction(nameof(Index));
            }


            return View(place);
        }

        // GET: Places/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Place place = null;
            try
            {
                place = await _placeServise.Find(id);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return NotFound();
            }

            if (place == null)
            {
                return NotFound();
            }

            return View(place);
        }

        // POST: Places/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            Place place = null;
            try
            {
                place = await _placeServise.Find(id);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return NotFound();
            }


            _placeServise.Delete(place);
            return RedirectToAction(nameof(Index));
        }

        private async Task<bool> PlaceExists(string id)
        {
            Place place = null;
            try
            {
                place = await _placeServise.Find(id);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }

            return place != null;
        }


        public IActionResult SearchResult(String key)
        {
            ViewBag.count = 0;
            int pageSize = 3;

            var places =  _placeServise.GetList()
                .Where(x => x.Name.Contains(key) || x.Description.Contains(key)).ToList();

            ViewBag.count = places.Count();
     
            return PartialView("Searchresult",places);

        }

        public async Task<IActionResult> FeedbackResult(String feedback, String placeId)
        {
            if (feedback == null)
            {
                var feedbacks = _feedbackServise.GetList().Where(x => x.PlaceId == placeId).ToList();
                ;
                return PartialView("FeedbackResult", SortDate(feedbacks));
            }
            if (!feedback.Equals(""))
            {
                Place place = null;
                AppliactionUser user = await _userManager.GetUserAsync(User);

                try
                {
                    place = await _placeServise.Find(placeId);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    return NotFound();
                }


                Feedback fb = new Feedback
                {
                    Place = place,
                    CreateAt = DateTime.Now,
                    PlaceId = place.Id,
                    Rating = 0,
                    Text = feedback,
                    User = user,
                    UserId = user.Id
                };

                fb = await _feedbackServise.Create(fb);


                var feedbacks = _feedbackServise.GetList().Where(x => x.PlaceId == placeId).ToList();
                ;
                return PartialView("FeedbackResult", SortDate(feedbacks));
            }

            return View();
        }

        public IEnumerable<Feedback> SortDate(IList<Feedback> readings)
        {
            var sortedReadings = readings.OrderByDescending(x => x.CreateAt.TimeOfDay)
                .ThenBy(x => x.CreateAt.Date)
                .ThenBy(x => x.CreateAt.Year);

            return sortedReadings;
        }

        public async Task<String> SetRating(int rating, String placeId)
        {
            Place place = null;


            if (rating ==0)
            {
                try
                {
                    place = await _placeServise.Find(placeId);
                    
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    return "";
                }
                return $"Rating {Math.Round(place.Rating / 3).ToString()}";
            }



            if (rating > 0)
            {

                

                try
                {
                    place = await _placeServise.Find(placeId);
                    
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    return "";
                }

                place.Rating += rating;
                bool done = await _placeServise.Update(place);
                if (done)
                {
                    return$"Rating {Math.Round(place.Rating / 3).ToString()}";
                }
            }
            return "";

        }





        //[HttpPost]
        //public async Task UploadFiles(IList<IFormFile> files)
        //{
        //    foreach (IFormFile file in files)
        //    {
        //        var path = Path.Combine(_environment.WebRootPath, $"images\\Gallery\\{Input.Email}");
        //        _fileUploadService.Upload(path, file.FileName, file);
        //        String imagePath = $"images/Gallery/{Input.Email}/{Input.ImageFile.FileName}";
        //    }
        //}
    }
}
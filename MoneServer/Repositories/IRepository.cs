﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MoneServer.Data.Entities;

namespace MoneServer.Repositories
{
    public interface IRepository<T> where T : Entity
    {
        IQueryable<T> GetList();
        Task<T> Create(T item);
        Task<bool> Update(T item);
        void Delete(T item);
        Task<T> Find(String id);
    }


    public interface IPlaceServise : IRepository<Place>
    {

    }

    public interface IGalleryServise : IRepository<Gallery>
    {

    }

    public interface IImageServise : IRepository<Image>
    {

    }

    public interface IFeedbackServise : IRepository<Feedback>
    {

    }
}

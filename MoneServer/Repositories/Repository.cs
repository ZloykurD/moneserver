﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using MoneServer.Data;
using MoneServer.Data.Entities;
using Newtonsoft.Json;

namespace MoneServer.Repositories
{
    public class Repository<T> : IRepository<T> where T : Entity
    {
        private readonly ApplicationDbContext db;

        public Repository(ApplicationDbContext db)
        {
            this.db = db;
        }

        protected DbSet<T> All { get; set; }

        public IQueryable<T> GetList()
        {
            return All;
        }

        public async Task<T> Create(T item)
        {
            try
            {
                EntityEntry<T> entry = await All.AddAsync(item);
                await db.SaveChangesAsync();
                item = entry.Entity;
            }
            catch (DbUpdateException ex)
            {
                Console.WriteLine(
                  $"Не удалось создать сущность {nameof(T)} с данными {JsonConvert.SerializeObject(item)} {Environment.NewLine}{ex}");
                throw;
            }

            return item;
        }

        public async Task<bool> Update(T item)
        {
            bool isUpdated = false;
            try
            {
                EntityEntry<T> entry = db.Update(item);
                await db.SaveChangesAsync();
                item = entry.Entity;
                isUpdated = true;
            }
            catch (DbUpdateException ex)
            {
                Console.WriteLine(
                  $"Не удалось обновить сущность {nameof(T)} с данными {JsonConvert.SerializeObject(item)} {Environment.NewLine}{ex}");
                throw;
            }

            return isUpdated;
        }

        public async void Delete(T item)
        {
            try
            {
                db.Remove(item);
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException ex)
            {
                Console.WriteLine(
                  $"Не удалось обновить сущность {nameof(T)} с данными {JsonConvert.SerializeObject(item)} {Environment.NewLine}{ex}");
                throw;
            }
        }

        public async Task<T> Find(string id)
        {
            T entity;
            try
            {
                entity = await All.FirstAsync(e => e.Id == id);
            }
            catch (InvalidOperationException ex)
            {
                Console.WriteLine($"Не удалось найти сущность {nameof(T)} по id {id} {Environment.NewLine}{ex}");
                throw;
            }

            return entity;
        }
    }

    public static class EntityExtensions
    {
        public static List<T> IncludeMultiple<T, TE>(this IQueryable<T> entities,
          params Expression<Func<T, IEnumerable<TE>>>[] includes)
          where TE : Entity where T : class
        {
            foreach (Expression<Func<T, IEnumerable<TE>>> expression in includes)
            {
                entities = entities.Include(expression);
            }

            return entities.ToList();
        }

        public static List<T> IncludeSingle<T, TE>(this IQueryable<T> entities,
          params Expression<Func<T, TE>>[] includes)
          where TE : Entity where T : class
        {
            foreach (Expression<Func<T, TE>> expression in includes)
            {
                entities = entities.Include(expression);
            }

            return entities.ToList();
        }
    }
}
